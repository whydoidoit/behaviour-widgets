"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.refreshOnStateChangeComplete = refreshOnStateChangeComplete;

var _reactIocWidgets = require("react-ioc-widgets");

var _alcumusBehaviours = require("alcumus-behaviours");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

(0, _alcumusBehaviours.register)("refreshOnStateChange", {
  methods: {
    stateChangeComplete: refreshOnStateChangeComplete
  }
});

function refreshOnStateChangeComplete() {
  _reactIocWidgets.globalWidgets.emit("update.".concat(this.document.behaviours.documentType, ".").concat(this.document.behaviours.id, ".").concat(this.document.behaviours.design.id));
}

function processEvent(context) {
  return context.event.split('.').slice(1).join(".");
}

function makeMethodName(type) {
  return type.split('.').join("_");
}

_reactIocWidgets.globalWidgets.onAny(function (event) {
  try {
    var _useDocument = (0, _reactIocWidgets.useDocument)(),
        _useDocument2 = _slicedToArray(_useDocument, 1),
        document = _useDocument2[0];

    if (document.behaviours) {
      var _document$behaviours;

      var baseEvent = event.split(".")[0];

      var _useDesign = (0, _reactIocWidgets.useDesign)(),
          _useDesign2 = _slicedToArray(_useDesign, 1),
          design = _useDesign2[0];

      var context = (0, _reactIocWidgets.useWidgetContext)();
      var params = {
        design: design,
        document: document,
        context: context
      };

      switch (baseEvent) {
        case "configure":
          var _useLayout = (0, _reactIocWidgets.useLayout)();

          var _useLayout2 = _slicedToArray(_useLayout, 2);

          params.layout = _useLayout2[0];
          params.setLayout = _useLayout2[1];
          break;

        case "render":
          var _useContent = (0, _reactIocWidgets.useContent)();

          var _useContent2 = _slicedToArray(_useContent, 2);

          params.content = _useContent2[0];
          params.setContent = _useContent2[1];
          break;

        case "context":
          var _useContextMenu = (0, _reactIocWidgets.useContextMenu)();

          var _useContextMenu2 = _slicedToArray(_useContextMenu, 2);

          params.menu = _useContextMenu2[0];
          params.addMenu = _useContextMenu2[1];
          break;

        case "editor":
          var _useTabs = (0, _reactIocWidgets.useTabs)();

          var _useTabs2 = _slicedToArray(_useTabs, 2);

          params.tabs = _useTabs2[0];
          params.addTab = _useTabs2[1];
          break;
      }

      for (var _len = arguments.length, values = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        values[_key - 1] = arguments[_key];
      }

      (_document$behaviours = document.behaviours).sendMessage.apply(_document$behaviours, [makeMethodName(event), params].concat(values));
    }
  } catch (e) {}
});

_reactIocWidgets.globalWidgets.configure("**", function () {
  var _useDocument3 = (0, _reactIocWidgets.useDocument)(),
      _useDocument4 = _slicedToArray(_useDocument3, 1),
      document = _useDocument4[0];

  if (document.behaviours) {
    var _document$behaviours2;

    var _useDesign3 = (0, _reactIocWidgets.useDesign)(),
        _useDesign4 = _slicedToArray(_useDesign3, 1),
        design = _useDesign4[0];

    var context = (0, _reactIocWidgets.useWidgetContext)();

    var _useLayout3 = (0, _reactIocWidgets.useLayout)(),
        _useLayout4 = _slicedToArray(_useLayout3, 2),
        layout = _useLayout4[0],
        setLayout = _useLayout4[1];

    document.behaviours.id = context.id;
    document.behaviours.documentType = context.documentType;
    document.behaviours.design = context.root;
    document.behaviours.context = context;

    for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      params[_key2] = arguments[_key2];
    }

    (_document$behaviours2 = document.behaviours).sendMessage.apply(_document$behaviours2, ["configure", processEvent(this), {
      design: design,
      document: document,
      context: context,
      layout: layout,
      setLayout: setLayout
    }].concat(params));
  }
});

_reactIocWidgets.globalWidgets.render("**", function () {
  var _useDocument5 = (0, _reactIocWidgets.useDocument)(),
      _useDocument6 = _slicedToArray(_useDocument5, 1),
      document = _useDocument6[0];

  if (document.behaviours) {
    var _document$behaviours3;

    var _useDesign5 = (0, _reactIocWidgets.useDesign)(),
        _useDesign6 = _slicedToArray(_useDesign5, 1),
        design = _useDesign6[0];

    var context = (0, _reactIocWidgets.useWidgetContext)();

    var _useContent3 = (0, _reactIocWidgets.useContent)(),
        _useContent4 = _slicedToArray(_useContent3, 2),
        content = _useContent4[0],
        setContent = _useContent4[1];

    for (var _len3 = arguments.length, params = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      params[_key3] = arguments[_key3];
    }

    (_document$behaviours3 = document.behaviours).sendMessage.apply(_document$behaviours3, ["render", processEvent(this), {
      design: design,
      document: document,
      context: context,
      content: content,
      setContent: setContent
    }].concat(params));
  }
});

_reactIocWidgets.globalWidgets.editor("**", function () {
  var _useDocument7 = (0, _reactIocWidgets.useDocument)(),
      _useDocument8 = _slicedToArray(_useDocument7, 1),
      document = _useDocument8[0];

  if (document.behaviours) {
    var _document$behaviours4;

    var _useDesign7 = (0, _reactIocWidgets.useDesign)(),
        _useDesign8 = _slicedToArray(_useDesign7, 1),
        design = _useDesign8[0];

    var context = (0, _reactIocWidgets.useWidgetContext)();

    var _useTabs3 = (0, _reactIocWidgets.useTabs)(),
        _useTabs4 = _slicedToArray(_useTabs3, 2),
        tabs = _useTabs4[0],
        addTab = _useTabs4[1];

    for (var _len4 = arguments.length, params = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      params[_key4] = arguments[_key4];
    }

    (_document$behaviours4 = document.behaviours).sendMessage.apply(_document$behaviours4, ["editor", processEvent(this), {
      design: design,
      document: document,
      context: context,
      tabs: tabs,
      addTab: addTab
    }].concat(params));
  }
});

_reactIocWidgets.globalWidgets.context("**", function () {
  var _useDocument9 = (0, _reactIocWidgets.useDocument)(),
      _useDocument10 = _slicedToArray(_useDocument9, 1),
      document = _useDocument10[0];

  if (document.behaviours) {
    var _document$behaviours5;

    var _useDesign9 = (0, _reactIocWidgets.useDesign)(),
        _useDesign10 = _slicedToArray(_useDesign9, 1),
        design = _useDesign10[0];

    var context = (0, _reactIocWidgets.useWidgetContext)();

    var _useContextMenu3 = (0, _reactIocWidgets.useContextMenu)(),
        _useContextMenu4 = _slicedToArray(_useContextMenu3, 2),
        menu = _useContextMenu4[0],
        addMenu = _useContextMenu4[1];

    for (var _len5 = arguments.length, params = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      params[_key5] = arguments[_key5];
    }

    (_document$behaviours5 = document.behaviours).sendMessage.apply(_document$behaviours5, ["context", processEvent(this), {
      design: design,
      document: document,
      context: context,
      menu: menu,
      addMenu: addMenu
    }].concat(params));
  }
});