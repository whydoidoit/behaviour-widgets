import {
    globalWidgets,
    useContent,
    useContextMenu,
    useDesign,
    useDocument,
    useLayout,
    useTabs,
    useWidgetContext
} from "react-ioc-widgets"

import {register} from "alcumus-behaviours"

register("refreshOnStateChange", {
    methods: {
        stateChangeComplete: refreshOnStateChangeComplete
    }
})

export function refreshOnStateChangeComplete() {
    globalWidgets.emit(`update.${this.document.behaviours.documentType}.${this.document.behaviours.id}.${this.document.behaviours.design.id}`)
}

function processEvent(context) {
    return context.event.split('.').slice(1).join(".")
}

function makeMethodName(type) {
    return type.split('.').join("_")
}

globalWidgets.onAny(function (event, ...values) {

    try {
        let [document] = useDocument()
        if (document.behaviours) {
            let baseEvent = event.split(".")[0]
            let [design] = useDesign()
            let context = useWidgetContext()
            let params = {
                design,
                document,
                context
            }
            switch (baseEvent) {
                case "configure":
                    [params.layout, params.setLayout] = useLayout()
                    break
                case "render":
                    [params.content, params.setContent] = useContent()
                    break
                case "context":
                    [params.menu, params.addMenu] = useContextMenu()
                    break
                case "editor":
                    [params.tabs, params.addTab] = useTabs()
                    break
            }
            document.behaviours.sendMessage(makeMethodName(event), params, ...values)
        }
    } catch (e) {

    }
})

globalWidgets.configure("**", function (...params) {
    let [document] = useDocument()
    if (document.behaviours) {
        let [design] = useDesign()
        let context = useWidgetContext()
        let [layout, setLayout] = useLayout()

        document.behaviours.id = context.id
        document.behaviours.documentType = context.documentType
        document.behaviours.design = context.root
        document.behaviours.context = context
        document.behaviours.sendMessage("configure", processEvent(this), {
            design,
            document,
            context,
            layout,
            setLayout
        }, ...params)
    }
})

globalWidgets.render("**", function (...params) {
    let [document] = useDocument()
    if (document.behaviours) {
        let [design] = useDesign()
        let context = useWidgetContext()
        let [content, setContent] = useContent()
        document.behaviours.sendMessage("render", processEvent(this), {
            design,
            document,
            context,
            content,
            setContent
        }, ...params)
    }
})

globalWidgets.editor("**", function (...params) {
    let [document] = useDocument()
    if (document.behaviours) {
        let [design] = useDesign()
        let context = useWidgetContext()
        let [tabs, addTab] = useTabs()
        document.behaviours.sendMessage("editor", processEvent(this), {
            design,
            document,
            context,
            tabs,
            addTab
        }, ...params)
    }
})

globalWidgets.context("**", function (...params) {
    let [document] = useDocument()
    if (document.behaviours) {
        let [design] = useDesign()
        let context = useWidgetContext()
        let [menu, addMenu] = useContextMenu()
        document.behaviours.sendMessage("context", processEvent(this), {
            design,
            document,
            context,
            menu,
            addMenu
        }, ...params)
    }
})
