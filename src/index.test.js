import React from "react"
import {useContent, useLayout, Widget, globalWidgets} from "react-ioc-widgets"
import Behaviours from "alcumus-behaviours"
import {refreshOnStateChangeComplete} from "./index"
import {render} from "react-ioc-widgets-test-renderer"

describe("Test behaviour add ons", function () {
    beforeEach(() => {
        Behaviours.reset()
    })
    it("should be able to add behaviours and they should call the configure functions", function () {
        const document = {}
        const design = {type: "test"}
        const configFn = jest.fn()
        Behaviours.register("test", {
            methods: {
                configure(type) {
                    expect(type).toEqual("test")
                },
                configure_test: configFn
            }
        })
        Behaviours.initialize(document)
        document.behaviours.add("test")
        expect(document.behaviours.instances.test).not.toBe(undefined)
        const {} = render({document, design})
        expect(configFn).toHaveBeenCalled()
    })
    it("should enable a configure function to add something", function () {
        const document = {}
        const design = {type: "test"}

        function Test() {
            return <div>Test Info</div>
        }

        Behaviours.register("test", {
            methods: {
                configure_test({setLayout}) {
                    setLayout({content: [Test]})
                }
            }
        })
        Behaviours.initialize(document)
        document.behaviours.add("test")
        const {getByText} = render({document, design})
        expect(getByText("Test Info"))
    })

    it("should be able to render a widget", function () {
        const document = {}
        const design = {type: "test"}
        const test = jest.fn()
        function Test() {
            return <div><Widget type="testWidget"/></div>
        }

        Behaviours.register("test", {
            methods: {
                stateChangeComplete: refreshOnStateChangeComplete,
                configure_test({setLayout}) {
                    test()
                    setLayout({content: [Test]})
                },
            },
            states: {
                default: {
                    methods: {
                        render_testWidget({setContent}) {
                            setContent(() => <div>Test Info</div>)
                        }
                    }
                },
                test: {
                    methods: {
                        render_testWidget({setContent}) {
                            setContent(() => <div>Test Again</div>)
                        },
                        configure_test({setLayout}) {
                            test()
                            setLayout({content: [Test]})
                        }
                    }
                }
            }
        })
        Behaviours.initialize(document)
        document.behaviours.add("test")
        let {getByText, setEditMode} = render({document, design})
        expect(getByText("Test Info"))
        document.behaviours.state = "test";
        setEditMode(true)
        setEditMode(false)
        expect(getByText("Test Again"))

    })
})
