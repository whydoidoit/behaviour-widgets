
module.exports = function (wallaby) {
    // Babel, jest-cli and some other modules may be located under
    // react-scripts/node_modules, so need to let node.js know about it
    var path = require('path');


    return {
        files: [
            'src/**/*.+(js|jsx|json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',
            'jest.config.js',
            '!src/**/*.test.js?(x)'
        ],

        tests: ['src/**/*.test.js?(x)'],

        env: {
            type: 'node',
            runner: 'node'
        },

        compilers: {
            '**/*.js?(x)': wallaby.compilers.babel({
                presets: ['react-app']
            })
        },

        setup: wallaby => {
            const jestConfig = require('./jest.config');
            Object.keys(jestConfig.transform || {}).forEach(k => ~k.indexOf('^.+\\.(js|jsx') && void delete jestConfig.transform[k]);
            delete jestConfig.testEnvironment;
            wallaby.testFramework.configure(jestConfig);
        },

        testFramework: 'jest'
    };
};
