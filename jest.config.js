module.exports = {
    testEnvironment: 'jsdom',
    testMatch: ['<rootDir>/src/**/*.test.js'],
    testPathIgnorePatterns: ['node_modules'],
    moduleDirectories: ["node_modules"],
    setupTestFrameworkScriptFile: "<rootDir>/src/setupTests.js",
    moduleNameMapper: {
        // "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
        "\\.(css|less)$": "identity-obj-proxy"
    }
};
